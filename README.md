![ASAFind Logo](ASAFindLogo.gif)  

ASAFind identifies nuclear-encoded plastid proteins in algae with secondary plastids of the red lineage based on the output of [SignalP][] and the identification of conserved "ASAFAP"-motifs and transit peptides. A web-based version of ASAFind can be found at [http://rocaplab.ocean.washington.edu/tools/asafind][ASAFind].


## Download
We recommend [Version 1.1.7][1.1.7].  
You may wish to use the latest [Development Version][latest] which is less tested.  
The published version is [Version 1.0.0][1.0.0].  
If you need another specific version, [click here][downloads] and click "tags."

## Installation Instructions
#### Install Prerequisites:

* [Python 2.7] or later.  
* [Biopython]. Developed and tested using v1.63. Subsequent versions are likely to work.

#### Install ASAFind:
Download and uncompress ASAFind. From the uncompressed directory, move `ASAFind.py` file to a directory in your PATH (such as /usr/local/bin). If your version of python is not at `/usr/bin/python` then adjust the first line of ASAFind.py to reflect your system's python path.


## Usage Tips
### Help
	ASAFind.py -h

### Inputs
Takes a Fasta and companion [SignalP][] file (versions 3.0, 4.0 or 4.1) short format file as input, either with the complete SignalP header (two lines starting with '#') or just with the results (first line starts with first sequence name). This tool was developed using 3.0, which remains the recommended version for algae with secondary plastids of the red lineage. Can be tested using the example input files: `ASAFind.py -f example.fasta -p example.signalp`. You can compare your example output to the expected output found in `expected_output.tab`.

### Sequence Names
[SignalP][] truncates the sequence names to 20 characters. Therefore, ASAFind only considers the first 20 characters of the fasta name, which must be unique within the file. Parts of the fasta name after the 20th character are ignored. Additionally, the fasta name may not contain `-` or `|` because SignalP changes them to `_`. If you discover any other characters that SignalP silently changes, [please let us know][mail us]

### Minimium Sequence Lengths
ASAFind requires at least 7 aa upstream and 22 aa downstream of the cleavage site identified by [SignalP][], no prediction is possible for sequences not meeting these requirements.


## Citation
If you use ASAFind in your research please cite our publication Gruber et al., [doi: 10.1111/tpj.12734][citation_ASAFind] as well as the publications for the appropriate [SignalP][] versions (Petersen et al., [doi: 10.1038/nmeth.1701][citation_SignalP4.1] for SignalP 4.1 or Bendtsen et al., [doi: 10.1016/j.jmb.2004.05.028][citation_SignalP3.0] for SignalP 3.0).


## Contact:
[cmckay@uw.edu][mail us]

[1.1.7]:https://bitbucket.org/rocaplab/asafind/get/1.1.7.zip
[1.0.0]:https://bitbucket.org/rocaplab/asafind/get/1.0.0.zip
[Python 2.7]:https://www.python.org/download/releases/2.7/
[Biopython]:http://biopython.org
[latest]:https://bitbucket.org/rocaplab/asafind/get/master.zip
[downloads]:https://bitbucket.org/rocaplab/asafind/downloads
[citation_ASAFind]:http://dx.doi.org/10.1111/tpj.12734
[citation_SignalP3.0]:http://dx.doi.org/10.1016/j.jmb.2004.05.028
[citation_SignalP4.1]:http://dx.doi.org/10.1038/nmeth.1701
[ASAFind]:http://rocaplab.ocean.washington.edu/tools/asafind
[mail us]: mailto:cmckay@uw.edu?subject=ASAFind
[SignalP]:http://www.cbs.dtu.dk/services/SignalP/
